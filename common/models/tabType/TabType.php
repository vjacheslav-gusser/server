<?php

namespace common\models\tabType;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%type}}".
 *
 * @property integer $id_row
 * @property integer $type
 * @property string $name_sum
 * @property integer $use_able
 *
 * @property AnketaDRL[] $anketaDRLs
 */
class TabType extends \yii\db\ActiveRecord
{
    const TYPE_INCOME = 1;
    const TYPE_FLOWRATE = 2;
    const TYPE_LOAN = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'name_sum'], 'required'],
            [['type', 'use_able'], 'integer'],
            [['name_sum'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_row' => 'ИД вида суммы',
            'type' => 'Тип вида суммы',
            'name_sum' => 'Наименование вида суммы',
            'use_able' => 'Доступность для выбора',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnketaDRLs()
    {
        return $this->hasMany(AnketaDRL::className(), ['tab_type_id_row' => 'id_row']);
    }

    /**
     * @inheritdoc
     * @return TabTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TabTypeQuery(get_called_class());
    }

    /**
     * @return ActiveDataProvider
     */
    public function getLoans()
    {
        return new ActiveDataProvider([
            'query' => self::find()->where(['type' => self::TYPE_LOAN]),
        ]);
    }

    /**
     * @return ActiveDataProvider
     */
    public function getFlowrates()
    {
        return new ActiveDataProvider([
            'query' => self::find()->where(['type' => self::TYPE_FLOWRATE]),
        ]);
    }

    /**
     * @return ActiveDataProvider
     */
    public function getIncomes()
    {
        return new ActiveDataProvider([
            'query' => self::find()->where(['type' => self::TYPE_INCOME]),
        ]);
    }
}
