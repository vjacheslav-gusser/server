<?php

namespace common\models\tabAnketa;

use common\models\tabAccounts\TabAccounts;
use Yii;
use yii\base\ExitException;

/**
 * This is the model class for table "{{%anketa}}".
 *
 * @property string $id_row
 * @property string $rec_date
 * @property string $tab_accounts_id_row
 * @property string $ad_r_index
 * @property string $ad_r_region
 * @property string $ad_r_district
 * @property string $ad_r_gorod
 * @property integer $ad_r_kod_ul
 * @property string $ad_r_ul
 * @property string $ad_r_dom
 * @property string $ad_r_str
 * @property string $ad_r_korp
 * @property string $ad_r_kv
 * @property string $ad_r_date
 * @property integer $ad_r_status
 * @property string $ad_f_index
 * @property string $ad_f_region
 * @property string $ad_f_district
 * @property string $ad_f_gorod
 * @property integer $ad_f_kod_ul
 * @property string $ad_f_ul
 * @property string $ad_f_dom
 * @property string $ad_f_str
 * @property string $ad_f_korp
 * @property string $ad_f_kv
 * @property string $ad_f_date
 * @property integer $ad_f_status
 * @property string $zpasp_nomer
 * @property string $zpasp_seria
 * @property string $zpasp_date
 * @property string $zpasp_vydan
 * @property string $zpasp_place
 * @property string $prava_nomer
 * @property string $prava_seria
 * @property string $prava_date
 * @property string $prava_vydan
 * @property string $snils
 * @property string $inn
 * @property string $oms
 * @property integer $marital_status
 * @property integer $num_dependants
 * @property integer $obrazov
 * @property integer $vid_work
 * @property integer $vid_biznes
 * @property integer $status_work
 * @property integer $staj
 * @property integer $staj_pf
 * @property string $doljn
 * @property string $work_telefon
 * @property string $firm_name
 * @property string $firm_index
 * @property string $firm_region
 * @property string $firm_district
 * @property string $firm_gorod
 * @property integer $firm_kod_ul
 * @property string $firm_ul
 * @property string $firm_dom
 * @property string $firm_str
 * @property string $firm_korp
 * @property string $firm_ofis
 * @property string $firm_telefon
 * @property integer $loans_past
 * @property integer $prosr_past
 * @property integer $loans_curr
 * @property integer $prosr_curr
 * @property string $sum_loans
 * @property string $sum_month
 * @property integer $own_estate
 * @property integer $own_avto
 *
 * @property Accounts $tabAccountsIdRow
 * @property OwnAvto $ownAvto
 * @property VidBiznes $vidBiznes
 * @property OwnEstate $ownEstate
 * @property Region $adFRegion
 * @property AdStatus $adFStatus
 * @property TipUl $adFKodUl
 * @property MaritalStatus $maritalStatus
 * @property Obrazov $obrazov0
 * @property Region $adRRegion
 * @property AdStatus $adRStatus
 * @property TipUl $adRKodUl
 * @property StatusWork $statusWork
 * @property VidWork $vidWork
 * @property Region $firmRegion
 * @property TipUl $firmKodUl
 * @property AnketaDRL[] $anketaDRLs
 */
class TabAnketa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%anketa}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rec_date', 'tab_accounts_id_row', 'ad_r_index', 'ad_r_region', 'ad_r_gorod', 'ad_r_kod_ul', 'ad_r_ul', 'ad_r_dom', 'ad_r_kv', 'ad_r_status', 'ad_f_index', 'ad_f_region', 'ad_f_gorod', 'ad_f_kod_ul', 'ad_f_ul', 'ad_f_dom', 'ad_f_kv', 'ad_f_status', 'marital_status', 'num_dependants', 'obrazov', 'vid_work'], 'required'],
            [['rec_date', 'ad_r_date', 'ad_f_date', 'zpasp_date', 'prava_date'], 'safe'],
            [['tab_accounts_id_row', 'ad_r_kod_ul', 'ad_r_status', 'ad_f_kod_ul', 'ad_f_status', 'marital_status', 'num_dependants', 'obrazov', 'vid_work', 'vid_biznes', 'status_work', 'staj', 'staj_pf', 'firm_kod_ul', 'loans_past', 'prosr_past', 'loans_curr', 'prosr_curr', 'own_estate', 'own_avto'], 'integer'],
            [['sum_loans', 'sum_month'], 'number'],
            [['ad_r_index', 'ad_f_index', 'prava_nomer', 'firm_index'], 'string', 'max' => 6],
            [['ad_r_region', 'ad_f_region', 'firm_region'], 'string', 'max' => 3],
            [['ad_r_district', 'ad_r_gorod', 'ad_r_ul', 'ad_f_district', 'ad_f_gorod', 'ad_f_ul', 'firm_district', 'firm_gorod', 'firm_ul'], 'string', 'max' => 80],
            [['ad_r_dom', 'ad_r_kv', 'ad_f_dom', 'ad_f_kv', 'firm_dom', 'firm_ofis'], 'string', 'max' => 40],
            [['ad_r_str', 'ad_r_korp', 'ad_f_str', 'ad_f_korp', 'firm_str', 'firm_korp'], 'string', 'max' => 10],
            [['zpasp_nomer'], 'string', 'max' => 7],
            [['zpasp_seria'], 'string', 'max' => 2],
            [['zpasp_vydan', 'prava_vydan'], 'string', 'max' => 510],
            [['zpasp_place'], 'string', 'max' => 1],
            [['prava_seria'], 'string', 'max' => 4],
            [['snils'], 'string', 'max' => 11],
            [['inn'], 'string', 'max' => 12],
            [['oms'], 'string', 'max' => 16],
            [['doljn'], 'string', 'max' => 100],
            [['work_telefon', 'firm_telefon'], 'string', 'max' => 15],
            [['firm_name'], 'string', 'max' => 170]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_row' => 'Номер строки (ИД версии анкеты)',
            'rec_date' => 'Дата и время записи версии',
            'tab_accounts_id_row' => 'ИД логина',
            'ad_r_index' => 'Индекс адреса регистрации',
            'ad_r_region' => 'Код региона адреса регистрации',
            'ad_r_district' => 'Район адреса регистрации',
            'ad_r_gorod' => 'Город адреса регистрации',
            'ad_r_kod_ul' => 'Код типа улицы адреса регистрации',
            'ad_r_ul' => 'Улица адреса регистрации',
            'ad_r_dom' => 'Дом адреса регистрации',
            'ad_r_str' => 'Строение адреса регистрации',
            'ad_r_korp' => 'Корпус адреса регистрации',
            'ad_r_kv' => 'Квартира адреса регистрации',
            'ad_r_date' => 'Дата регистрации на адресе',
            'ad_r_status' => 'Код статуса адреса регистрации',
            'ad_f_index' => 'Индекс фактического адреса',
            'ad_f_region' => 'Код региона фактического адреса',
            'ad_f_district' => 'Район фактического адреса',
            'ad_f_gorod' => 'Город фактического адреса',
            'ad_f_kod_ul' => 'Код типа улицы фактического адреса',
            'ad_f_ul' => 'Улица фактического адреса',
            'ad_f_dom' => 'Дом фактического адреса',
            'ad_f_str' => 'Строение фактического адреса',
            'ad_f_korp' => 'Корпус фактического адреса',
            'ad_f_kv' => 'Квартира фактического адреса',
            'ad_f_date' => 'Дата регистрации на фактическом адресе',
            'ad_f_status' => 'Код статуса фактического адреса',
            'zpasp_nomer' => 'Номер загранпаспорта',
            'zpasp_seria' => 'Серия загранпаспорта',
            'zpasp_date' => 'Дата выдачи загранпаспорта',
            'zpasp_vydan' => 'Кем выдан загранпаспорт',
            'zpasp_place' => 'Место выдачи загранпаспорта',
            'prava_nomer' => 'Номер загранпаспорта',
            'prava_seria' => 'Серия загранпаспорта',
            'prava_date' => 'Дата выдачи загранпаспорта',
            'prava_vydan' => 'Кем выдан загранпаспорт',
            'snils' => 'Номер пенсионного страхования (СНИЛС)',
            'inn' => 'ИНН',
            'oms' => 'Номер ОМС',
            'marital_status' => 'Код семейного положения',
            'num_dependants' => 'Количество иждивенцев',
            'obrazov' => 'Код образования',
            'vid_work' => 'Код вида занятости',
            'vid_biznes' => 'Код вида деятельности',
            'status_work' => 'Код статуса работы',
            'staj' => 'Стаж на последнем месте работы (месяцев)',
            'staj_pf' => 'Стаж в данной профессиональной области (месяцев)',
            'doljn' => 'Должность',
            'work_telefon' => 'Рабочий телефон',
            'firm_name' => 'Название организации',
            'firm_index' => 'Индекс адреса организации',
            'firm_region' => 'Код региона адреса организации',
            'firm_district' => 'Район адреса организации',
            'firm_gorod' => 'Город адреса организации',
            'firm_kod_ul' => 'Код типа улицы адреса организации',
            'firm_ul' => 'Улица адреса организации',
            'firm_dom' => 'Дом адреса организации',
            'firm_str' => 'Строение адреса организации',
            'firm_korp' => 'Корпус адреса организации',
            'firm_ofis' => 'Офис адреса организации',
            'firm_telefon' => 'Телефон организации',
            'loans_past' => 'Наличие прошлых кредитов',
            'prosr_past' => 'Наличие просрочек в прошлом',
            'loans_curr' => 'Наличие текущих кредитов',
            'prosr_curr' => 'Наличие текущих просрочек',
            'sum_loans' => 'Сумма всех кредитов',
            'sum_month' => 'Сумма ежемесячного гашения',
            'own_estate' => 'Владение недвижимостью',
            'own_avto' => 'Владение автомобилем',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(TabAccounts::className(), ['id_row' => 'tab_accounts_id_row']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwnAvto()
    {
        return $this->hasOne(OwnAvto::className(), ['kod_avto' => 'own_avto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVidBiznes()
    {
        return $this->hasOne(VidBiznes::className(), ['kod_biznes' => 'vid_biznes']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwnEstate()
    {
        return $this->hasOne(OwnEstate::className(), ['kod_estate' => 'own_estate']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdFRegion()
    {
        return $this->hasOne(Region::className(), ['kod_region' => 'ad_f_region']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdFStatus()
    {
        return $this->hasOne(AdStatus::className(), ['kod_ad_status' => 'ad_f_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdFKodUl()
    {
        return $this->hasOne(TipUl::className(), ['kod_tip_ul' => 'ad_f_kod_ul']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaritalStatus()
    {
        return $this->hasOne(MaritalStatus::className(), ['kod_marital' => 'marital_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObrazov0()
    {
        return $this->hasOne(Obrazov::className(), ['kod_obrazov' => 'obrazov']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdRRegion()
    {
        return $this->hasOne(Region::className(), ['kod_region' => 'ad_r_region']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdRStatus()
    {
        return $this->hasOne(AdStatus::className(), ['kod_ad_status' => 'ad_r_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdRKodUl()
    {
        return $this->hasOne(TipUl::className(), ['kod_tip_ul' => 'ad_r_kod_ul']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusWork()
    {
        return $this->hasOne(StatusWork::className(), ['kod_status_work' => 'status_work']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVidWork()
    {
        return $this->hasOne(VidWork::className(), ['kod_work' => 'vid_work']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirmRegion()
    {
        return $this->hasOne(Region::className(), ['kod_region' => 'firm_region']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirmKodUl()
    {
        return $this->hasOne(TipUl::className(), ['kod_tip_ul' => 'firm_kod_ul']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnketaDRLs()
    {
        return $this->hasMany(AnketaDRL::className(), ['tab_anketa_id_row' => 'id_row']);
    }

    /**
     * @inheritdoc
     * @return TabAnketaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TabAnketaQuery(get_called_class());
    }

    /**
     * @param array $attributes
     * @param TabAccounts $account
     * @return bool
     * @throws ExitException
     */
    public function createWorksheet(array $attributes, TabAccounts $account)
    {
        $this->load($attributes);
        $this->link('account', $account);

        if ($this->getIsNewRecord()) {
            throw new ExitException('Не удалось создать анкету');
        } else {
            return true;
        }
    }
}
