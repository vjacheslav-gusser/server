<?php

namespace frontend\controllers\rosters;

use frontend\controllers\mixins\Cors;
use yii\rest\ActiveController;

/**
 * Class StreetTypesController
 * @package frontend\controllers\rosters
 */
class StreetTypesController extends ActiveController
{
    use Cors;
    /**
     * @var string
     */
    public $modelClass = 'common\models\tabTipUl\TabTipUl';
}