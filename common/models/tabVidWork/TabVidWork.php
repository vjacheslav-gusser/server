<?php

namespace common\models\tabVidWork;

use Yii;

/**
 * This is the model class for table "{{%vid_work}}".
 *
 * @property integer $kod_work
 * @property string $name_work
 * @property integer $use_able
 *
 * @property Anketa[] $anketas
 */
class TabVidWork extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vid_work}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kod_work', 'name_work'], 'required'],
            [['kod_work', 'use_able'], 'integer'],
            [['name_work'], 'string', 'max' => 60]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kod_work' => 'Код вида занятости',
            'name_work' => 'Наименование вида занятости',
            'use_able' => 'Доступность для выбора',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnketas()
    {
        return $this->hasMany(Anketa::className(), ['vid_work' => 'kod_work']);
    }

    /**
     * @inheritdoc
     * @return TabVidWorkQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TabVidWorkQuery(get_called_class());
    }
}
