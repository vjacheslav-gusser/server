<?php

namespace common\models\tabStatusWork;

use Yii;

/**
 * This is the model class for table "{{%status_work}}".
 *
 * @property integer $kod_status_work
 * @property string $name_status_work
 * @property integer $use_able
 *
 * @property Anketa[] $anketas
 */
class TabStatusWork extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%status_work}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kod_status_work', 'name_status_work'], 'required'],
            [['kod_status_work', 'use_able'], 'integer'],
            [['name_status_work'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kod_status_work' => 'Код статуса работы',
            'name_status_work' => 'Наименование статуса работы',
            'use_able' => 'Доступность для выбора',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnketas()
    {
        return $this->hasMany(Anketa::className(), ['status_work' => 'kod_status_work']);
    }

    /**
     * @inheritdoc
     * @return TabStatusWorkQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TabStatusWorkQuery(get_called_class());
    }
}
