<?php

namespace frontend\controllers\rosters;

use frontend\controllers\mixins\Cors;

/**
 * Class OwnEstateController
 * @package frontend\controllers\rosters
 */
class OwnEstateController extends \yii\rest\ActiveController
{
    use Cors;
    /**
     * @var string
     */
    public $modelClass = 'common\models\tabOwnEstate\TabOwnEstate';
}
