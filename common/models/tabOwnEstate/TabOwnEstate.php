<?php

namespace common\models\tabOwnEstate;

use Yii;

/**
 * This is the model class for table "{{%own_estate}}".
 *
 * @property integer $kod_estate
 * @property string $name_estate
 * @property integer $use_able
 *
 * @property Anketa[] $anketas
 */
class TabOwnEstate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%own_estate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kod_estate', 'name_estate'], 'required'],
            [['kod_estate', 'use_able'], 'integer'],
            [['name_estate'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kod_estate' => 'Код владения недвижимостью',
            'name_estate' => 'Наименование владения недвижимостью',
            'use_able' => 'Доступность для выбора',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnketas()
    {
        return $this->hasMany(Anketa::className(), ['own_estate' => 'kod_estate']);
    }

    /**
     * @inheritdoc
     * @return TabOwnEstateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TabOwnEstateQuery(get_called_class());
    }
}
