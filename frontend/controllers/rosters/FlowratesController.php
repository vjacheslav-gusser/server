<?php

namespace frontend\controllers\rosters;

use common\models\tabType\TabType;
use frontend\controllers\mixins\Cors;

/**
 * Class FlowratesController
 * @package frontend\controllers\rosters
 */
class FlowratesController extends \yii\rest\Controller
{
    use Cors;

    /**
     * @return \yii\data\ActiveDataProvider
     */
    public function actionIndex()
    {
        return (new TabType())->getFlowrates();
    }
}
