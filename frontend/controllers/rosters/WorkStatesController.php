<?php

namespace frontend\controllers\rosters;

use frontend\controllers\mixins\Cors;

/**
 * Class WorkStatesController
 * @package frontend\controllers\rosters
 */
class WorkStatesController extends \yii\rest\ActiveController
{
    use Cors;
    /**
     * @var string
     */
    public $modelClass = 'common\models\tabStatusWork\TabStatusWork';
}
