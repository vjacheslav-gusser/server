<?php

namespace frontend\controllers;

use common\models\tabAccounts\TabAccounts;
use common\models\tabAnketa\TabAnketa;
use common\models\tabAnketaDRL\TabAnketaDRL;
use common\models\tabPhonesConfirmed\TabPhonesConfirmed;
use frontend\controllers\mixins\Cors;
use yii\base\ExitException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\ServerErrorHttpException;

/**
 * Class RegistrationController
 * @package frontend\controllers
 */
class RegistrationController extends \yii\rest\Controller
{
    use Cors;

    /**
     * @return array
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ]);
    }

    /**
     * Так как ангуляр отправляет данные в видео JSON'а, а PHP
     * их самостоятельно не обрабатывает, приходится получать
     * данные не из суперглобальных массивов, а из потока
     *
     * @return mixed
     * @throws \Exception
     */
    public function actionCreate()
    {
        $transaction = \Yii::$app->db->transaction(function() {
            $request = Json::decode(\Yii::$app->request->getRawBody());

            try {
                $account = new TabAccounts();
                $account->createAccount($request);

                $worksheet = new TabAnketa();
                $worksheet->createWorksheet($request, $account);

                $drl = new TabAnketaDRL();
                $drl->addItemsWithRegistration($request['TabAnketaDRL'], $worksheet);

                $phoneConfirm = new TabPhonesConfirmed();
                $phoneConfirm->createSmsCode($worksheet);

                return $phoneConfirm;
            } catch (ExitException $ex) {
                throw new ServerErrorHttpException($ex->getMessage());
            }
        });

        return $transaction;
    }
}
