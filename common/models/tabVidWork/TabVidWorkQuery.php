<?php

namespace common\models\tabVidWork;

/**
 * This is the ActiveQuery class for [[TabVidWork]].
 *
 * @see TabVidWork
 */
class TabVidWorkQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TabVidWork[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TabVidWork|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}