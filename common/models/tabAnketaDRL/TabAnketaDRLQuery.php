<?php

namespace common\models\tabAnketaDRL;

/**
 * This is the ActiveQuery class for [[TabAnketaDRL]].
 *
 * @see TabAnketaDRL
 */
class TabAnketaDRLQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TabAnketaDRL[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TabAnketaDRL|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}