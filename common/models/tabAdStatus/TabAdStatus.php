<?php

namespace common\models\tabAdStatus;

use Yii;

/**
 * This is the model class for table "{{%tab_ad_status}}".
 *
 * @property integer $kod_ad_status
 * @property string $name_ad_status
 * @property integer $use_able
 *
 * @property TabAnketa[] $tabAnketas
 * @property TabAnketa[] $tabAnketas0
 */
class TabAdStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ad_status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kod_ad_status', 'name_ad_status'], 'required'],
            [['kod_ad_status', 'use_able'], 'integer'],
            [['name_ad_status'], 'string', 'max' => 65]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kod_ad_status' => 'Код статуса адреса',
            'name_ad_status' => 'Наименование статуса адреса',
            'use_able' => 'Доступность для выбора',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTabAnketas()
    {
        return $this->hasMany(TabAnketa::className(), ['ad_f_status' => 'kod_ad_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTabAnketas0()
    {
        return $this->hasMany(TabAnketa::className(), ['ad_r_status' => 'kod_ad_status']);
    }

    /**
     * @inheritdoc
     * @return TabAdStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TabAdStatusQuery(get_called_class());
    }
}
