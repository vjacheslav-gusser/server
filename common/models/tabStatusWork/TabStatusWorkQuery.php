<?php

namespace common\models\tabStatusWork;

/**
 * This is the ActiveQuery class for [[TabStatusWork]].
 *
 * @see TabStatusWork
 */
class TabStatusWorkQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TabStatusWork[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TabStatusWork|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}