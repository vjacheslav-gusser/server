<?php

namespace frontend\controllers\rosters;

use common\models\tabType\TabType;
use frontend\controllers\mixins\Cors;

/**
 * Class IncomesController
 * @package frontend\controllers\rosters
 */
class IncomesController extends \yii\rest\Controller
{
    use Cors;

    /**
     * @return \yii\data\ActiveDataProvider
     */
    public function actionIndex()
    {
        return (new TabType())->getIncomes();
    }
}
