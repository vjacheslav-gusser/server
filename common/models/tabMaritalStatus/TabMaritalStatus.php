<?php

namespace common\models\tabMaritalStatus;

use Yii;

/**
 * This is the model class for table "{{%marital_status}}".
 *
 * @property integer $kod_marital
 * @property string $name_marital
 * @property integer $use_able
 *
 * @property Anketa[] $anketas
 */
class TabMaritalStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%marital_status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kod_marital', 'name_marital'], 'required'],
            [['kod_marital', 'use_able'], 'integer'],
            [['name_marital'], 'string', 'max' => 25]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kod_marital' => 'Код семейного положения',
            'name_marital' => 'Наименование семейного положения',
            'use_able' => 'Доступность для выбора',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnketas()
    {
        return $this->hasMany(Anketa::className(), ['marital_status' => 'kod_marital']);
    }

    /**
     * @inheritdoc
     * @return TabMaritalStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TabMaritalStatusQuery(get_called_class());
    }
}
