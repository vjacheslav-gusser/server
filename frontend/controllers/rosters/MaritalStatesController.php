<?php

namespace frontend\controllers\rosters;
use frontend\controllers\mixins\Cors;

/**
 * Class MaritalStatesController
 * @package frontend\controllers\rosters
 */
class MaritalStatesController extends \yii\rest\ActiveController
{
    use Cors;
    /**
     * @var string
     */
    public $modelClass = 'common\models\tabMaritalStatus\TabMaritalStatus';
}
