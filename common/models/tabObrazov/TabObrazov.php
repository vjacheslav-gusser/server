<?php

namespace common\models\tabObrazov;

use Yii;

/**
 * This is the model class for table "{{%obrazov}}".
 *
 * @property integer $kod_obrazov
 * @property string $name_obrazov
 * @property integer $use_able
 *
 * @property Anketa[] $anketas
 */
class TabObrazov extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%obrazov}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kod_obrazov', 'name_obrazov'], 'required'],
            [['kod_obrazov', 'use_able'], 'integer'],
            [['name_obrazov'], 'string', 'max' => 40]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kod_obrazov' => 'Код образования',
            'name_obrazov' => 'Наименование образования',
            'use_able' => 'Доступность для выбора',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnketas()
    {
        return $this->hasMany(Anketa::className(), ['obrazov' => 'kod_obrazov']);
    }

    /**
     * @inheritdoc
     * @return TabObrazovQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TabObrazovQuery(get_called_class());
    }
}
