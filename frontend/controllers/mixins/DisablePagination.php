<?php

namespace frontend\controllers\mixins;

use yii\data\ActiveDataProvider;

/**
 * Class DisablePagination
 * @package frontend\controllers\mixins
 */
trait DisablePagination
{
    /**
     * @return mixed
     */
    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = function(/** @var \yii\rest\IndexAction $action */$action) {
            /** @var \yii\db\ActiveRecord $modelClass */
            $modelClass = $action->modelClass;
            return new ActiveDataProvider([
                'query' => $modelClass::find(),
                'pagination' => false,
            ]);
        };

        return $actions;
    }
}