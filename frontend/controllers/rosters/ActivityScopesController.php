<?php

namespace frontend\controllers\rosters;

use frontend\controllers\mixins\Cors;

/**
 * Class ActivityScopesController
 * @package frontend\controllers\rosters
 */
class ActivityScopesController extends \yii\rest\ActiveController
{
    use Cors;
    /**
     * @var string
     */
    public $modelClass = 'common\models\tabVidBiznes\TabVidBiznes';
}
