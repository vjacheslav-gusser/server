<?php

namespace common\models\tabVidBiznes;

/**
 * This is the ActiveQuery class for [[TabVidBiznes]].
 *
 * @see TabVidBiznes
 */
class TabVidBiznesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TabVidBiznes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TabVidBiznes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}