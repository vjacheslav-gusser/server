<?php

namespace frontend\controllers;

use common\models\tabPhonesConfirmed\TabPhonesConfirmed;
use frontend\controllers\mixins\Cors;
use yii\rest\ActiveController;

use yii\web\ServerErrorHttpException;

class PhoneConfirmController extends ActiveController
{
    use Cors;
    /**
     * @var string
     */
    public $modelClass = 'common\models\tabPhonesConfirmed\TabPhonesConfirmed';

    /**
     * @param $id
     * @return null|static
     * @throws ServerErrorHttpException
     */
    public function actionCheck($id)
    {
        $token = TabPhonesConfirmed::findOne([
            'id_row' => $id,
            'token' => \Yii::$app->request->getQueryParam('token')
        ]);

        if (!$token) {
            throw new ServerErrorHttpException('Invalid token');
        }

        return $token;
    }

    /**
     * @param $id
     * @throws ServerErrorHttpException
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionConfirm($id)
    {
        $request = \Yii::$app->request->getQueryParams();
        /** @var \common\models\tabPhonesConfirmed\TabPhonesConfirmed $token */
        $token = TabPhonesConfirmed::findOne(['id_row' => $id, 'token' => $request['token']]);

        if (!$token) {
            throw new ServerErrorHttpException('Not found');
        }

        if ($token->isValidCode($request['smsCode'])) {
            if ($token->delete() === false) {
                \Yii::$app->getResponse()->setStatusCode(203);
            }
        } else {
            throw new ServerErrorHttpException('Invalid sms code');
        }
    }
}
