<?php

namespace common\models\tabObrazov;

/**
 * This is the ActiveQuery class for [[TabObrazov]].
 *
 * @see TabObrazov
 */
class TabObrazovQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TabObrazov[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TabObrazov|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}