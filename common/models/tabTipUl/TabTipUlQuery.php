<?php

namespace common\models\tabTipUl;

/**
 * This is the ActiveQuery class for [[TabTipUl]].
 *
 * @see TabTipUl
 */
class TabTipUlQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TabTipUl[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TabTipUl|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}