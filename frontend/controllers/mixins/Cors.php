<?php

namespace frontend\controllers\mixins;

/**
 * Class Cors
 * @package frontend\controllers\mixins
 */
trait Cors
{
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['corsFilter'] = [
            'class' => 'yii\filters\Cors',
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Request-Method' => ['DELETE', 'POST', 'GET', 'HEAD', 'OPTIONS'],
            ],
        ];

        return $behaviors;
    }
}