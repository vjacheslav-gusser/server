<?php

namespace frontend\controllers\rosters;

use frontend\controllers\mixins\Cors;

/**
 * Class OwnCarController
 * @package frontend\controllers\rosters
 */
class OwnCarController extends \yii\rest\ActiveController
{
    use Cors;
    /**
     * @var string
     */
    public $modelClass = 'common\models\tabOwnAvto\TabOwnAvto';
}
