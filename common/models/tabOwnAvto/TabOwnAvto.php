<?php

namespace common\models\tabOwnAvto;

use Yii;

/**
 * This is the model class for table "{{%own_avto}}".
 *
 * @property integer $kod_avto
 * @property string $name_avto
 * @property integer $use_able
 *
 * @property Anketa[] $anketas
 */
class TabOwnAvto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%own_avto}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kod_avto', 'name_avto'], 'required'],
            [['kod_avto', 'use_able'], 'integer'],
            [['name_avto'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kod_avto' => 'Код владения автотранспортом',
            'name_avto' => 'Наименование владения автотранспортом',
            'use_able' => 'Доступность для выбора',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnketas()
    {
        return $this->hasMany(Anketa::className(), ['own_avto' => 'kod_avto']);
    }

    /**
     * @inheritdoc
     * @return TabOwnAvtoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TabOwnAvtoQuery(get_called_class());
    }
}
