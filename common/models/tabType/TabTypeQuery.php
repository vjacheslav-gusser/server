<?php

namespace common\models\tabType;

/**
 * This is the ActiveQuery class for [[TabType]].
 *
 * @see TabType
 */
class TabTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TabType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TabType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}