<?php

namespace frontend\controllers\rosters;

use common\models\tabType\TabType;
use frontend\controllers\mixins\Cors;

/**
 * Class LoanTypesController
 * @package frontend\controllers\rosters
 */
class LoanTypesController extends \yii\rest\Controller
{
    use Cors;

    /**
     * @return array
     */
    public function verbs()
    {
        return [
            'index' => ['get']
        ];
    }

    /**
     * @return static[]
     */
    public function actionIndex()
    {
        return (new TabType())->getLoans();
    }
}
