<?php

namespace frontend\controllers;

use yii\rest\Controller;

class ApiController extends Controller
{
    public function verbs()
    {
        return [
            'index' => ['get'],
        ];
    }

    public function actionIndex()
    {
        return [
            'name' => 'PortalVk rest api',
            'version' => 1.0,
            'link' => \Yii::$app->getRequest()->getHostInfo(),
        ];
    }
}