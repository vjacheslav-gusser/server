<?php

namespace common\models\tabAdStatus;

/**
 * This is the ActiveQuery class for [[TabAdStatus]].
 *
 * @see TabAdStatus
 */
class TabAdStatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TabAdStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TabAdStatus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}