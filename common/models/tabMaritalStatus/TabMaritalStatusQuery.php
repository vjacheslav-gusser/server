<?php

namespace common\models\tabMaritalStatus;

/**
 * This is the ActiveQuery class for [[TabMaritalStatus]].
 *
 * @see TabMaritalStatus
 */
class TabMaritalStatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TabMaritalStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TabMaritalStatus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}