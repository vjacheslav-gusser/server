<?php

use yii\db\Schema;
use yii\db\Migration;

class m151103_082539_create_phones_confirmed_table extends Migration
{
    /**
     * @var string
     */
    private $table = '{{%phones_confirmed}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id_row' => Schema::TYPE_PK,
            'worksheet' => Schema::TYPE_INTEGER.' unsigned',
            'token' => $this->string(128)->notNull()->unique(),
            'code' => $this->integer()->notNull()
        ]);

        $this->addForeignKey('worksheet_fk', $this->table, 'worksheet', '{{%anketa}}', 'id_row', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
