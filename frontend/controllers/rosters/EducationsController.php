<?php

namespace frontend\controllers\rosters;

use frontend\controllers\mixins\Cors;

/**
 * Class EducationsController
 * @package frontend\controllers\rosters
 */
class EducationsController extends \yii\rest\ActiveController
{
    use Cors;
    /**
     * @var string
     */
    public $modelClass = 'common\models\tabObrazov\TabObrazov';
}
