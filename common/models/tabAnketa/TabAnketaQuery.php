<?php

namespace common\models\tabAnketa;

/**
 * This is the ActiveQuery class for [[TabAnketa]].
 *
 * @see TabAnketa
 */
class TabAnketaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TabAnketa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TabAnketa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}