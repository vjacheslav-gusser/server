<?php

namespace frontend\controllers\rosters;

use frontend\controllers\mixins\Cors;

/**
 * Class WorkTypesController
 * @package frontend\controllers\rosters
 */
class WorkTypesController extends \yii\rest\ActiveController
{
    use Cors;
    /**
     * @var string
     */
    public $modelClass = 'common\models\tabVidWork\TabVidWork';
}
