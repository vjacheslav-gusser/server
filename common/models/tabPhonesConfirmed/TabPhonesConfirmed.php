<?php

namespace common\models\tabPhonesConfirmed;

use common\models\tabAnketa\TabAnketa;
use Yii;
use yii\base\ExitException;

/**
 * This is the model class for table "{{%phones_confirmed}}".
 *
 * @property integer $id_row
 * @property string $worksheet
 * @property string $token
 * @property integer $code
 * @property integer $isConfirmed
 *
 * @property Anketa $worksheet0
 */
class TabPhonesConfirmed extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%phones_confirmed}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worksheet', 'code'], 'integer'],
            [['token', 'code'], 'required'],
            [['token'], 'string', 'max' => 128],
            [['token'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_row' => 'Id Row',
            'worksheet' => 'Worksheet',
            'token' => 'Token',
            'code' => 'Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorksheet()
    {
        return $this->hasOne(TabAnketa::className(), ['id_row' => 'worksheet']);
    }

    /**
     * @inheritdoc
     * @return TabPhonesConfirmedQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TabPhonesConfirmedQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function fields()
    {
        return ['id_row', 'token'];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->fillAttributes();
            return true;
        } else {
            return false;
        }
    }

    protected function fillAttributes()
    {
        $this->token = \Yii::$app->security->generateRandomString();
        $this->code = rand(1000, 9999);
    }

    /**
     * @param TabAnketa $worksheet
     * @return bool
     * @throws ExitException
     */
    public function createSmsCode(TabAnketa $worksheet)
    {
        $this->link('worksheet', $worksheet);

        if ($this->getIsNewRecord()) {
            throw new ExitException('Не удалось создать код подтверждения');
        } else {
            return true;
        }
    }

    /**
     * @param $code
     * @return bool
     */
    public function isValidCode($code)
    {
        return $this->code === (int) $code;
    }
}
