<?php

namespace common\models\tabAccounts;

use common\models\tabAnketa\TabAnketa;
use Yii;
use yii\base\ExitException;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%accounts}}".
 *
 * @property string $id_row
 * @property string $login_acc
 * @property string $password
 * @property string $name_1
 * @property string $name_2
 * @property string $name_3
 * @property string $birth
 * @property string $place_birth
 * @property integer $gender
 * @property string $telefon
 * @property string $e_mail
 * @property string $pasp_nomer
 * @property string $pasp_seria
 * @property string $pasp_date
 * @property string $pasp_vydan
 * @property string $pasp_place
 * @property string $pasp_kod_podr
 * @property string $open_date
 * @property string $close_date
 * @property string $accessToken
 * @property string $authKey
 *
 * @property Anketa[] $anketas
 */
class TabAccounts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%accounts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login_acc', 'password', 'name_1', 'name_2', 'birth', 'place_birth', 'gender', 'telefon', 'e_mail', 'pasp_nomer', 'pasp_seria', 'pasp_date', 'pasp_vydan', 'pasp_place', 'pasp_kod_podr'], 'required'],
            [['birth', 'pasp_date', 'open_date', 'close_date'], 'safe'],
            [['gender'], 'integer'],
            [['login_acc', 'password'], 'string', 'max' => 128],
            [['name_1', 'name_2', 'name_3'], 'string', 'max' => 60],
            [['place_birth'], 'string', 'max' => 500],
            [['telefon'], 'string', 'max' => 15],
            [['e_mail'], 'string', 'max' => 100],
            [['pasp_nomer'], 'string', 'max' => 6],
            [['pasp_seria'], 'string', 'max' => 4],
            [['pasp_vydan', 'pasp_place'], 'string', 'max' => 255],
            [['pasp_kod_podr'], 'string', 'max' => 7],
            [['accessToken', 'authKey'], 'string', 'max' => 255],
            [['login_acc'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_row' => 'Номер строки (ИД логина)',
            'login_acc' => 'Логин',
            'password' => 'Пароль',
            'name_1' => 'Фамилия',
            'name_2' => 'Имя',
            'name_3' => 'Отчество',
            'birth' => 'Дата рождения',
            'place_birth' => 'Место рождения',
            'gender' => 'Пол',
            'telefon' => 'Номер мобильного телефона',
            'e_mail' => 'E-mail',
            'pasp_nomer' => 'Номер паспорта',
            'pasp_seria' => 'Серия паспорта',
            'pasp_date' => 'Дата выдачи',
            'pasp_vydan' => 'Кем выдан',
            'pasp_place' => 'Место выдачи',
            'pasp_kod_podr' => 'Код подразделения',
            'open_date' => 'Дата и время открытия аккаунта',
            'close_date' => 'Дата и время закрытия аккаунта',
            'accessToken' => 'Access Token',
            'authKey' => 'Auth Key',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'password',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'password',
                ],
                'value' => function () {
                    return \Yii::$app->security->generatePasswordHash($this->password);
                }
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorksheet()
    {
        return $this->hasMany(TabAnketa::className(), ['tab_accounts_id_row' => 'id_row']);
    }

    /**
     * @inheritdoc
     * @return TabAccountsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TabAccountsQuery(get_called_class());
    }

    /**
     * @param array $attributes
     * @return bool
     * @throws ExitException
     */
    public function createAccount(array $attributes)
    {
        if ($this->load($attributes) && $this->save()) {
            return true;
        } else {
            throw new ExitException('Не удалось создать аккаунт');
        }
    }
}
