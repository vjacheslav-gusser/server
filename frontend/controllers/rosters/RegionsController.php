<?php

namespace frontend\controllers\rosters;

use frontend\controllers\mixins\Cors;
use frontend\controllers\mixins\DisablePagination;

/**
 * Class RegionsController
 * @package frontend\controllers\rosters
 */
class RegionsController extends \yii\rest\ActiveController
{
    use Cors, DisablePagination;
    /**
     * @var string
     */
    public $modelClass = 'common\models\tabRegion\TabRegion';
}
