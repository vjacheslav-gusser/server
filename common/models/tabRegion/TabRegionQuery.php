<?php

namespace common\models\tabRegion;

/**
 * This is the ActiveQuery class for [[TabRegion]].
 *
 * @see TabRegion
 */
class TabRegionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TabRegion[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TabRegion|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}