<?php

namespace common\models\tabRegion;

use Yii;

/**
 * This is the model class for table "{{%region}}".
 *
 * @property string $kod_region
 * @property string $name_region
 * @property integer $use_able
 *
 * @property Anketa[] $anketas
 * @property Anketa[] $anketas0
 * @property Anketa[] $anketas1
 */
class TabRegion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%region}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kod_region', 'name_region'], 'required'],
            [['use_able'], 'integer'],
            [['kod_region'], 'string', 'max' => 3],
            [['name_region'], 'string', 'max' => 80]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kod_region' => 'Код региона',
            'name_region' => 'Наименование региона',
            'use_able' => 'Доступность для выбора',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnketas()
    {
        return $this->hasMany(Anketa::className(), ['ad_f_region' => 'kod_region']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnketas0()
    {
        return $this->hasMany(Anketa::className(), ['ad_r_region' => 'kod_region']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnketas1()
    {
        return $this->hasMany(Anketa::className(), ['firm_region' => 'kod_region']);
    }

    /**
     * @inheritdoc
     * @return TabRegionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TabRegionQuery(get_called_class());
    }
}
