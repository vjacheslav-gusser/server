<?php

namespace common\models\tabOwnAvto;

/**
 * This is the ActiveQuery class for [[TabOwnAvto]].
 *
 * @see TabOwnAvto
 */
class TabOwnAvtoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TabOwnAvto[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TabOwnAvto|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}