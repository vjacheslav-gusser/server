<?php

namespace frontend\controllers\rosters;

use frontend\controllers\mixins\Cors;
use yii\rest\ActiveController;

/**
 * Class AddressStatesController
 * @package frontend\controllers\rosters
 */
class AddressStatesController extends ActiveController
{
    use Cors;
    /**
     * @var string
     */
    public $modelClass = 'common\models\tabAdStatus\TabAdStatus';
}
