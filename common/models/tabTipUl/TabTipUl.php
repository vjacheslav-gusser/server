<?php

namespace common\models\tabTipUl;

use Yii;

/**
 * This is the model class for table "{{%tab_tip_ul}}".
 *
 * @property integer $kod_tip_ul
 * @property string $short_tip_ul
 * @property string $name_tip_ul
 * @property integer $use_able
 *
 * @property TabAnketa[] $tabAnketas
 * @property TabAnketa[] $tabAnketas0
 * @property TabAnketa[] $tabAnketas1
 */
class TabTipUl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tip_ul}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kod_tip_ul', 'short_tip_ul', 'name_tip_ul'], 'required'],
            [['kod_tip_ul', 'use_able'], 'integer'],
            [['short_tip_ul'], 'string', 'max' => 15],
            [['name_tip_ul'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kod_tip_ul' => 'Код типа улицы',
            'short_tip_ul' => 'Краткое наименование типа улицы',
            'name_tip_ul' => 'Наименование типа улицы',
            'use_able' => 'Доступность для выбора',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTabAnketas()
    {
        return $this->hasMany(TabAnketa::className(), ['ad_f_kod_ul' => 'kod_tip_ul']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTabAnketas0()
    {
        return $this->hasMany(TabAnketa::className(), ['ad_r_kod_ul' => 'kod_tip_ul']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTabAnketas1()
    {
        return $this->hasMany(TabAnketa::className(), ['firm_kod_ul' => 'kod_tip_ul']);
    }

    /**
     * @inheritdoc
     * @return TabTipUlQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TabTipUlQuery(get_called_class());
    }
}
