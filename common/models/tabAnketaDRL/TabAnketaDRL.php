<?php

namespace common\models\tabAnketaDRL;

use common\models\tabAnketa\TabAnketa;
use common\models\tabType\TabType;
use Yii;
use yii\base\ExitException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%anketa_d_r_l}}".
 *
 * @property string $id_row
 * @property string $tab_anketa_id_row
 * @property integer $tab_type_type
 * @property integer $tab_type_id_row
 * @property string $sum
 *
 * @property \common\models\tabAnketa\TabAnketa $tabAnketaIdRow
 * @property \common\models\tabType\TabType $tabTypeIdRow
 */
class TabAnketaDRL extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%anketa_d_r_l}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tab_anketa_id_row', 'tab_type_id_row', 'sum'], 'required'],
            [['tab_anketa_id_row', 'tab_type_id_row'], 'integer'],
            [['sum'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_row' => 'Номер строки',
            'tab_anketa_id_row' => 'Версия анкеты',
            'tab_type_type' => 'Тип вида суммы',
            'tab_type_id_row' => 'Код вида суммы',
            'sum' => 'Сумма',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorksheet()
    {
        return $this->hasOne(TabAnketa::className(), ['id_row' => 'tab_anketa_id_row']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTabTypeIdRow()
    {
        return $this->hasOne(TabType::className(), ['id_row' => 'tab_type_id_row']);
    }

    /**
     * @inheritdoc
     * @return TabAnketaDRLQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TabAnketaDRLQuery(get_called_class());
    }

    /**
     * @param array $containers
     * @param TabAnketa $worksheet
     */
    public function addItemsWithRegistration(array $containers, TabAnketa $worksheet)
    {
        if (count($containers['loans']) > 0) {
            $this->batchItems($this->linkToWorksheet($containers['loans'], $worksheet));
        }

        if (count($containers['incomes']) > 0) {
            $this->batchItems($this->linkToWorksheet($containers['incomes'], $worksheet));
        }

        if (count($containers['flowrates']) > 0) {
            $this->batchItems($this->linkToWorksheet($containers['flowrates'], $worksheet));
        }
    }

    /**
     * @param array $items
     * @param TabAnketa $worksheet
     * @return array
     */
    private function linkToWorksheet(array $items, TabAnketa $worksheet)
    {
        foreach ($items as &$item) {
            $item['tab_anketa_id_row'] = $worksheet->id_row;
            $item['tab_type_type'] = TabType::findOne(['id_row' => $item['tab_type_id_row']])->type;
        }

        return $items;
    }

    /**
     * @param array $items
     * @return int
     * @throws \yii\db\Exception
     */
    public function batchItems(array $items)
    {
        return $this->getDb()->createCommand()
            ->batchInsert(self::tableName(), ['tab_type_id_row', 'sum', 'tab_anketa_id_row', 'tab_type_type'], $items)
            ->execute();
    }
}
