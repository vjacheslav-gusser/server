<?php

namespace common\models\tabVidBiznes;

use Yii;

/**
 * This is the model class for table "{{%vid_biznes}}".
 *
 * @property integer $kod_biznes
 * @property string $name_biznes
 * @property integer $use_able
 *
 * @property Anketa[] $anketas
 */
class TabVidBiznes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vid_biznes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kod_biznes', 'name_biznes'], 'required'],
            [['kod_biznes', 'use_able'], 'integer'],
            [['name_biznes'], 'string', 'max' => 80]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kod_biznes' => 'Код вида деятельности',
            'name_biznes' => 'Наименование вида деятельности',
            'use_able' => 'Доступность для выбора',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnketas()
    {
        return $this->hasMany(Anketa::className(), ['vid_biznes' => 'kod_biznes']);
    }

    /**
     * @inheritdoc
     * @return TabVidBiznesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TabVidBiznesQuery(get_called_class());
    }
}
